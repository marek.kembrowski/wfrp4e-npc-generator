## 3.1.0

### New features

- Possibilité de drag and drop un folder d'acteur sur l'input genPath pour remplir le champs

## 3.0.2

### Bugfix

- Bug sur le choix des abilitées dans le parsing des acteurs

## 3.0.1

### Bugfix

- Les créatures sont maintenant correctement généré avec leur Hit Location Table
- Les Pnj sont correctement généré avec leur image
- Les Pnj sont correctement généré avec la race au format species (subspecies)
- L'option générer dans un répertoire nom de carrière est prise en compte même si l'option path de génération est vide

## 3.0.0

### Technical

- Grosse refonte technique des cartes de choix (utilisation d'Handlebar au lieu de DialogRef)
- Compatibilité V9
- Patch temporaire pour améliorer la compatibilité avec Nation of Mankind, Ogre kingdom et Dwark Empire
  (Attention, l'utilisation de ces modules reste instable)

### Ergonomics

- Changement dans la façon de choisir des carrières
- Ajout d'icônes pour distinguer les items monde et compendium
- Ajout des icônes d'items dans les différentes listes de choix

### New features

- Création de créatures avec le template PNJ (avec ajout de carrière)
- Groupement des mutations par dieux du chaos sur l'écran d'ajout des mutations

### Bugfix

- Bug d'affichage du nom du monde dans la liste de choix de créature
- Gestion temporaire (en attendant la traduction) des talents de carrières à choix exclusif
