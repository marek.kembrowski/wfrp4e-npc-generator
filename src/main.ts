import TrappingUtil from './util/trapping-util'
import RegisterSettings from './util/register-settings'
import {
  actors,
  i18n,
  initTemplates,
  isGM,
  user,
  wfrp4e,
} from './constant'
import { speciesReferential } from './referential/species-referential'
import { NpcGenerator } from './generators/npc/npc-generator'
import { CreatureGenerator } from './generators/creature/creature-generator'
import ReferentialUtil from './util/referential-util'
import { FoundryActor, FoundryToken } from './types/foundry/actor'

Hooks.once('init', () => {
  wfrp4e().npcGen = NpcGenerator
  wfrp4e().creatureGen = CreatureGenerator
  wfrp4e().speciesReferencial = speciesReferential

  RegisterSettings.initSettings()

  Handlebars.registerHelper('json', (context) => {
    return JSON.stringify(context)
  })

  initTemplates([
    `modules/${RegisterSettings.moduleName}/templates/generation-profiles.html`,
    `modules/${RegisterSettings.moduleName}/templates/species-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/chooser-action.html`,
    `modules/${RegisterSettings.moduleName}/templates/career-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/species-skills-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/species-others-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/name-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/options-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/abilities-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/ability-rows.html`,
    `modules/${RegisterSettings.moduleName}/templates/ability-row.html`,
    `modules/${RegisterSettings.moduleName}/templates/trapping-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/magic-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/magic-rows.html`,
    `modules/${RegisterSettings.moduleName}/templates/magic-row.html`,
    `modules/${RegisterSettings.moduleName}/templates/mutation-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/mutation-rows.html`,
    `modules/${RegisterSettings.moduleName}/templates/mutation-row.html`,
    `modules/${RegisterSettings.moduleName}/templates/creature-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/creature-abilities-chooser.html`,
    `modules/${RegisterSettings.moduleName}/templates/creature-ability-rows.html`,
    `modules/${RegisterSettings.moduleName}/templates/creature-ability-row.html`,
    `modules/${RegisterSettings.moduleName}/templates/creature-ability-attr.html`,
    `modules/${RegisterSettings.moduleName}/templates/creature-ability-toggle-attr.html`,
    `modules/${RegisterSettings.moduleName}/templates/creature-ability-select-attr.html`,
    `modules/${RegisterSettings.moduleName}/templates/world-option-icon.html`,
    `modules/${RegisterSettings.moduleName}/templates/world-label.html`,
    `modules/${RegisterSettings.moduleName}/templates/creature-pnj-chooser.html`,
  ])
})

Hooks.on('getActorDirectoryEntryContext', async (_, options) => {
  options.push(
    {
      name: i18n().localize('WFRP4NPCGEN.generate.copy.npc'),
      condition: isGM(),
      icon: '<i class="fas fa-copy"></i>',
      callback: async (target) => {
        await ReferentialUtil.initReferential(() => {
          generateFromWorld(target)
        }, true)
      },
    },
    {
      name: i18n().localize('WFRP4NPCGEN.generate.copy.creature'),
      condition: isGM(),
      icon: '<i class="fas fa-copy"></i>',
      callback: async (target) => {
        await ReferentialUtil.initReferential(() => {
          generateFromWorld(target, true)
        }, true)
      },
    },
    {
      name: i18n().localize('WFRP4NPCGEN.generate.money'),
      condition: isGM(),
      icon: '<i class="fas fa-coins"></i>',
      callback: async (target) => {
        await generateMoney(target)
      },
    },
  )
})

Hooks.on('getCompendiumEntryContext', async (compendium, options) => {
  const label = compendium.metadata.id
  options.push({
    name: i18n().localize('WFRP4NPCGEN.compendium.generate.npc'),
    condition: isGM(),
    icon: '<i class="fas fa-download"></i>',
    callback: async (target) => {
      await ReferentialUtil.initReferential(() => {
        generateFromCompendium(target, label as string)
      }, true)
    },
  })
  options.push({
    name: i18n().localize('WFRP4NPCGEN.compendium.generate.creature'),
    condition: isGM(),
    icon: '<i class="fas fa-download"></i>',
    callback: async (target) => {
      await ReferentialUtil.initReferential(() => {
        generateFromCompendium(target, label as string, true)
      }, true)
    },
  })
})

Hooks.on('renderActorDirectory', (_app, html: JQuery) => {
  if (user().can('ACTOR_CREATE')) {
    addActorActionButton(html, 'WFRP4NPCGEN.creature.directory.button', () => {
      CreatureGenerator.generateCreature()
    })
    addActorActionButton(html, 'WFRP4NPCGEN.actor.directory.button', () => {
      NpcGenerator.generateNpc()
    })
  }
})

Hooks.on('createToken', async (token: FoundryToken) => {
  const scene = token.parent
  const actor: FoundryActor = actors()?.tokens[token.id]
  if (token?.actorLink || actor == null) {
    return
  }
  const generateMoneyEffect = actor.effects?.find(
    (eff) => eff.label === i18n().localize('WFRP4NPCGEN.trappings.money.label'),
  )
  const generateWeaponEffect = actor.effects?.find(
    (eff) =>
      eff.label === i18n().localize('WFRP4NPCGEN.trappings.weapon.label'),
  )
  const updateScene =
    (generateMoneyEffect != null && !generateMoneyEffect.disabled) ||
    (generateWeaponEffect != null && !generateWeaponEffect.disabled)

  if (generateMoneyEffect != null && !generateMoneyEffect.disabled) {
    await TrappingUtil.generateMoney(actor)
  }

  if (generateWeaponEffect != null && !generateWeaponEffect.disabled) {
    await TrappingUtil.generateWeapons(actor)
  }

  if (updateScene) {
    await scene.updateEmbeddedDocuments(Token.embeddedName, [actor])
  }
})

function addActorActionButton(
  html: JQuery,
  label: string,
  onClick: () => void,
) {
  const button = document.createElement('button')
  button.style.width = '95%'
  button.innerHTML = i18n().localize(label)
  button.addEventListener('click', () => {
    onClick()
  })
  html.find('.header-actions').after(button)
}

async function generateMoney(target: { attr: (key: string) => string }) {
  const actor: FoundryActor = game.actors.get(target.attr('data-document-id'))

  await TrappingUtil.generateMoney(actor)
}

async function generateFromWorld(
  target: { attr: (key: string) => string },
  creature = false,
) {
  const actor: FoundryActor = game.actors.get(target.attr('data-document-id'))

  if (creature) {
    await CreatureGenerator.generateFromActor(actor)
  } else {
    await NpcGenerator.generateFromActor(actor)
  }
}

async function generateFromCompendium(
  target: { attr: (key: string) => string },
  label: string,
  creature = false,
) {
  const actorsMap = await ReferentialUtil.getActorsEntities(false)
  const actor = actorsMap[label].find(
    (a) => a.id === target.attr('data-document-id'),
  )

  if (creature) {
    await CreatureGenerator.generateFromActor(actor as FoundryActor)
  } else {
    await NpcGenerator.generateFromActor(actor as FoundryActor)
  }
}
