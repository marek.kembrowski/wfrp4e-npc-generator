import ReferentialUtil from './referential-util'
import StringUtil from './string-util'
import RandomUtil from './random-util'
import { i18n, wfrp4e } from '../constant'
import EntityUtil from './entity-util'
import { FoundryActor } from '../types/foundry/actor'
import { FoundryTrapping } from '../types/foundry/trapping'
import { FoundrySkill } from '../types/foundry/skill'

export default class TrappingUtil {
  public static readonly UPDATE_QUANTITY_KEY = 'system.quantity.value'
  public static readonly UPDATE_SKILL_NAME_KEY = 'name'

  public static async initMoney(actor: FoundryActor) {
    if (actor == null) {
      return
    }

    const coins = actor.itemTypes?.money ?? []
    let gCoin = coins.find((c) => c?.system?.coinValue?.value === 240)
    let sCoin = coins.find((c) => c?.system?.coinValue?.value === 12)
    let bCoin = coins.find((c) => c?.system?.coinValue?.value === 1)

    if (gCoin != null && actor.updateEmbeddedDocuments) {
      await actor.updateEmbeddedDocuments(Item.metadata.name, [
        {
          _id: gCoin.id,
          [this.UPDATE_QUANTITY_KEY]: 0,
        },
      ])
    }

    if (sCoin != null && actor.updateEmbeddedDocuments) {
      await actor.updateEmbeddedDocuments(Item.metadata.name, [
        {
          _id: sCoin.id,
          [this.UPDATE_QUANTITY_KEY]: 0,
        },
      ])
    }

    if (bCoin != null && actor.updateEmbeddedDocuments) {
      await actor.updateEmbeddedDocuments(Item.metadata.name, [
        {
          _id: bCoin.id,
          [this.UPDATE_QUANTITY_KEY]: 0,
        },
      ])
    }
  }

  public static async generateMoney(actor: FoundryActor) {
    if (actor == null) {
      return
    }

    const actorStatus = actor?.system?.details?.status?.value

    const statusTiers: {
      [key: string]: string
    } = ReferentialUtil.getStatusTiers()

    const status = actorStatus?.split(' ')[0]
    const tierStr = actorStatus?.split(' ')[1]
    let tier: number =
      tierStr != null && Number.isNumeric(tierStr) ? Number(tierStr) : -1
    let statusKey: string | null = null
    Object.entries(statusTiers)?.forEach(([key, value]) => {
      if (value === status) {
        statusKey = key
      }
    })

    if (tier < 0) {
      tier = RandomUtil.getRandomValue([1, 2, 3, 4, 5])
    }

    if (statusKey == null) {
      statusKey = RandomUtil.getRandomValue(['g', 's', 'b'])
    }

    let gold = 0
    let silver = 0
    let brass: number

    if (statusKey === 'g') {
      gold = tier
      silver = await RandomUtil.roll('5d10')
      brass = await RandomUtil.roll('10d10')
    } else if (statusKey === 's') {
      silver = await RandomUtil.roll(`${tier}d10`)
      brass = await RandomUtil.roll('10d10')
    } else {
      brass = await RandomUtil.roll(`${2 * tier}d10`)
    }

    const moneyItems = await ReferentialUtil.getAllMoneyItems()
    let coins = actor.itemTypes?.money ?? []
    let gCoin = coins.find((c) => c?.system?.coinValue?.value === 240)
    let sCoin = coins.find((c) => c?.system?.coinValue?.value === 12)
    let bCoin = coins.find((c) => c?.system?.coinValue?.value === 1)

    const isGoldCreate = gCoin == null
    const isSilverCreate = sCoin == null
    const isBrassCreate = bCoin == null

    const createGCoin = moneyItems[0]
    const createSCoin = moneyItems[1]
    const createBCoin = moneyItems[2]

    const coinsToCreates: FoundryTrapping[] = []

    if (gold > 0 || isGoldCreate) {
      if (isGoldCreate) {
        createGCoin.system.quantity.value = gold
        coinsToCreates.push(createGCoin)
      } else if (actor.updateEmbeddedDocuments) {
        await actor.updateEmbeddedDocuments(Item.metadata.name, [
          {
            _id: gCoin.id,
            [this.UPDATE_QUANTITY_KEY]: gold + gCoin.system.quantity.value,
          },
        ])
      }
    }

    if (silver > 0 || isSilverCreate) {
      if (isSilverCreate) {
        createSCoin.system.quantity.value = silver
        coinsToCreates.push(createSCoin)
      } else if (actor.updateEmbeddedDocuments) {
        await actor.updateEmbeddedDocuments(Item.metadata.name, [
          {
            _id: sCoin.id,
            [this.UPDATE_QUANTITY_KEY]: silver + sCoin.system.quantity.value,
          },
        ])
      }
    }

    if (brass > 0 || isBrassCreate) {
      if (isBrassCreate) {
        createBCoin.system.quantity.value = brass
        coinsToCreates.push(createBCoin)
      } else if (actor.updateEmbeddedDocuments) {
        await actor.updateEmbeddedDocuments(Item.metadata.name, [
          {
            _id: bCoin.id,
            [this.UPDATE_QUANTITY_KEY]: brass + bCoin.system.quantity.value,
          },
        ])
      }
    }

    if (coinsToCreates.length > 0 && actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toRecords(coinsToCreates),
      )
    }

    const money = actor.itemTags ? actor.itemTags['money'] : []
    const newMoney = wfrp4e().market.consolidateMoney(
      money.map((i) => i.toObject()),
    )
    if (actor.updateEmbeddedDocuments) {
      await actor.updateEmbeddedDocuments(Item.metadata.name, newMoney)
    }
  }

  public static async generateWeapons(actor: FoundryActor) {
    if (actor == null) {
      return
    }

    const groups: string[] = []

    const weaponSkills: FoundrySkill[] =
      actor.itemTypes?.skill
        ?.filter(
          (i: FoundrySkill) =>
            i.name.includes('(') &&
            (StringUtil.includesDeburrIgnoreCase(
              i.name,
              ReferentialUtil.getWeaponTypes().melee,
            ) ||
              StringUtil.includesDeburrIgnoreCase(
                i.name,
                ReferentialUtil.getWeaponTypes().ranged,
              )),
        )
        .sort((s1: FoundrySkill, s2: FoundrySkill) => {
          const s1HaveAny = StringUtil.includesDeburrIgnoreCase(
            s1.name,
            i18n().localize('WFRP4NPCGEN.item.any'),
          )
          const s2HaveAny = StringUtil.includesDeburrIgnoreCase(
            s2.name,
            i18n().localize('WFRP4NPCGEN.item.any'),
          )
          if (s1HaveAny && s2HaveAny) {
            return 0
          } else if (s1HaveAny) {
            return 1
          } else if (s2HaveAny) {
            return -1
          }
          return 0
        }) ?? []
    for (let skill of weaponSkills) {
      const isMelee = StringUtil.includesDeburrIgnoreCase(
        skill.name,
        ReferentialUtil.getWeaponTypes().melee,
      )
      let group = skill.name.substring(
        skill.name.indexOf('(') + 1,
        skill.name.indexOf(')'),
      )
      let replaceSkill = false
      if (
        !StringUtil.arrayIncludesDeburrIgnoreCase(
          ReferentialUtil.getWeaponGroups(),
          group,
        )
      ) {
        group = RandomUtil.getRandomValue(
          isMelee
            ? ReferentialUtil.getMeleeWeaponGroups()
            : ReferentialUtil.getRangedWeaponGroups(),
          groups,
        )
        if (group == null) {
          group = ReferentialUtil.getBasicWeaponGroups()
        }
        console.warn(
          `Unknown weapon group from skill ${skill.name}, resolved by random ${group}`,
        )
        replaceSkill = true
      }

      const existingCount =
        actor.itemTypes?.weapon?.filter((w) =>
          StringUtil.equalsDeburrIgnoreCase(
            w?.system?.weaponGroup?.value,
            group,
          ),
        )?.length ?? 0

      const ignore =
        (StringUtil.equalsDeburrIgnoreCase(
          group,
          ReferentialUtil.getBasicWeaponGroups(),
        ) &&
          existingCount > 1) ||
        existingCount > 0

      if (!ignore && !groups.includes(group)) {
        groups.push(group)
        if (replaceSkill && actor.updateEmbeddedDocuments) {
          await actor.updateEmbeddedDocuments(Item.metadata.name, [
            {
              _id: skill._id,
              [this.UPDATE_SKILL_NAME_KEY]: `${
                isMelee
                  ? ReferentialUtil.getWeaponTypes().melee
                  : ReferentialUtil.getWeaponTypes().ranged
              } (${group})`,
            },
          ])
        }
      }
    }

    const itemsToCreate: FoundryTrapping[] = []
    const addWeapons: FoundryTrapping[] = []

    if (groups.length > 0) {
      const weapons = (await ReferentialUtil.getTrappingEntities(true)).filter(
        (w) => w.type === 'weapon',
      )

      for (let group of groups) {
        const randomWeapon = RandomUtil.getRandomValue(
          weapons.filter((w) =>
            StringUtil.equalsDeburrIgnoreCase(
              w?.system?.weaponGroup?.value,
              ReferentialUtil.getWeaponGroupsKey(group),
            ),
          ),
        )

        addWeapons.push(randomWeapon)
        itemsToCreate.push(randomWeapon)
      }
    }

    const allWeapons = [...(actor.itemTypes?.weapon ?? []), ...addWeapons]

    if (allWeapons.length > 0) {
      const ammunitions = (
        await ReferentialUtil.getTrappingEntities(true)
      ).filter((w) => w.type === 'ammunition')

      for (let weapons of allWeapons) {
        const ammunitionGroup = weapons?.system?.ammunitionGroup?.value
        if (ammunitionGroup != null) {
          const randomAmmunition = RandomUtil.getRandomValue(
            ammunitions.filter((w) =>
              StringUtil.equalsDeburrIgnoreCase(
                w?.system?.ammunitionType?.value,
                ammunitionGroup,
              ),
            ),
          )
          if (randomAmmunition != null) {
            const quantity = randomAmmunition?.system?.quantity?.value
            if (quantity != null && quantity < 10) {
              randomAmmunition.system.quantity.value = 10
            }
            itemsToCreate.push(randomAmmunition)
          }
        }
      }
    }

    if (itemsToCreate.length > 0 && actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toDuplicateRecords(itemsToCreate),
      )
    }
  }
}
