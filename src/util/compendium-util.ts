import WaiterUtil from './waiter-util'
import { i18n, notifications, packs, wfrp4e } from '../constant'
import { FoundryCareer } from '../types/foundry/career'
import { FoundryItem } from '../types/foundry/item'
import { FoundryActor } from '../types/foundry/actor'
import { FoundryTrapping } from '../types/foundry/trapping'
import { FoundrySkill } from '../types/foundry/skill'
import { FoundryTalent } from '../types/foundry/talent'
import { FoundryTrait } from '../types/foundry/trait'
import { FoundryPsychology } from '../types/foundry/psychology'
import { FoundrySpell } from '../types/foundry/spell'
import { FoundryPrayer } from '../types/foundry/prayer'
import { FoundryMutation } from '../types/foundry/mutation'
import { FoundryPack } from '../types/foundry/game'
import { FoundryTable } from '../types/foundry/table'
import NotificationUtil from './notification-util'

export default class CompendiumUtil {
  private static compendiumItems: FoundryItem[]
  private static compendiumActors: { [pack: string]: FoundryActor[] }
  private static compendiumCareers: FoundryCareer[]
  private static compendiumCareerGroups: string[]
  private static compendiumTrappings: FoundryTrapping[]
  private static compendiumBestiary: { [pack: string]: FoundryActor[] }
  private static compendiumSkills: FoundrySkill[]
  private static compendiumTalents: FoundryTalent[]
  private static compendiumTraits: FoundryTrait[]
  private static compendiumPsychologies: FoundryPsychology[]
  private static compendiumSizeTrait: FoundryTrait
  private static compendiumSwarmTrait: FoundryTrait
  private static compendiumWeaponTrait: FoundryTrait
  private static compendiumArmorTrait: FoundryTrait
  private static compendiumRangedTrait: FoundryTrait
  private static compendiumSpells: FoundrySpell[]
  private static compendiumPrayers: FoundryPrayer[]
  private static compendiumPhysicalMutations: FoundryMutation[]
  private static compendiumMentalMutations: FoundryMutation[]
  private static compendiumTables: FoundryTable[]

  private static compendiumLoaded = false
  private static creatureCompendiumLoaded = false

  public static async initCompendium(
    callback: () => void,
    forCreatures = false,
  ) {
    if (!this.compendiumLoaded || !this.creatureCompendiumLoaded) {
      await WaiterUtil.show(
        'WFRP4NPCGEN.compendium.load.title',
        'WFRP4NPCGEN.compendium.load.hint',
        async () => {
          try {
            NotificationUtil.silent = true
            if (forCreatures) {
              await Promise.all([
                this.getCompendiumItems(),
                this.getCompendiumTables(),
                this.getCompendiumActors(),
              ])
              await Promise.all([
                this.getCompendiumTrappings(),
                this.getCompendiumBestiary(),
                this.getCompendiumSkills(),
                this.getCompendiumTalents(),
                this.getCompendiumTraits(),
              ])
              await Promise.all([
                this.getCompendiumSizeTrait(),
                this.getCompendiumSwarmTrait(),
                this.getCompendiumWeaponTrait(),
                this.getCompendiumArmourTrait(),
                this.getCompendiumRangedTrait(),
              ])
            } else {
              await this.getCompendiumItems()
              await this.getCompendiumTables()
              await Promise.all([
                this.getCompendiumCareers(),
                this.getCompendiumTrappings(),
              ])
              await this.getCompendiumCareersGroups()
            }
          } finally {
            NotificationUtil.silent = false
          }

          if (!this.compendiumLoaded || !this.creatureCompendiumLoaded) {
            await WaiterUtil.hide()
          }
          if (forCreatures) {
            this.creatureCompendiumLoaded = true
          } else {
            this.compendiumLoaded = true
          }

          callback()
        },
      )
    } else {
      callback()
    }
  }

  public static async getCompendiumItems() {
    if (this.compendiumItems == null) {
      this.compendiumItems = []
      const itemsPacks = packs().filter((p) => p.documentName === 'Item')
      const loaders: Promise<FoundryItem[]>[] = []
      for (let pack of itemsPacks) {
        loaders.push(this.loadCompendiumDocuments(pack))
      }
      const contents = await Promise.all(loaders)
      for (let items of contents) {
        this.compendiumItems.push(...items)
      }
    }
    return Promise.resolve(this.compendiumItems)
  }

  public static async getCompendiumTables() {
    if (this.compendiumTables == null) {
      this.compendiumTables = []
      const itemsPacks = packs().filter((p) => p.documentName === 'RollTable')
      const loaders: Promise<FoundryTable[]>[] = []
      for (let pack of itemsPacks) {
        loaders.push(this.loadCompendiumDocuments(pack))
      }
      const contents = await Promise.all(loaders)
      for (let items of contents) {
        this.compendiumTables.push(...items)
      }
    }
    return Promise.resolve(this.compendiumTables)
  }

  public static async getCompendiumActors() {
    if (this.compendiumActors == null) {
      this.compendiumActors = {}

      const actorsPacks = packs().filter(
        (p: FoundryPack) => p.documentName === 'Actor',
      )

      const packLoader: (pack: FoundryPack) => Promise<boolean> = (
        pack: FoundryPack,
      ) => {
        return new Promise(async (resolve) => {
          let key = pack.metadata.id

          console.info(`Start to load ${key} compendium`)

          const actors: FoundryActor[] = (
            await this.loadCompendiumDocuments<FoundryActor[]>(pack)
          ).sort((c1: FoundryActor, c2: FoundryActor) => {
            return c1.name?.localeCompare(c2.name ?? '')
          })

          if (actors.length > 0) {
            this.compendiumActors[key] = actors
          }

          console.info(`End to load ${key} compendium`)

          resolve(true)
        })
      }
      const loaders: Promise<boolean>[] = []
      for (let pack of actorsPacks) {
        loaders.push(packLoader(pack))
      }
      await Promise.all(loaders)
    }
    return Promise.resolve(this.compendiumActors)
  }

  public static async getCompendiumCareers() {
    if (this.compendiumCareers == null) {
      this.compendiumCareers = []
      this.compendiumCareers.push(
        ...(await this.getCompendiumItems()).filter(
          (c: FoundryItem) => c.type === 'career',
        ),
      )
    }
    return Promise.resolve(this.compendiumCareers)
  }

  public static async getCompendiumCareersGroups() {
    if (this.compendiumCareerGroups == null) {
      this.compendiumCareerGroups = []
      const careers = await this.getCompendiumCareers()
      for (let career of careers) {
        const group = career?.system?.careergroup?.value
        if (group != null && !this.compendiumCareerGroups.includes(group)) {
          this.compendiumCareerGroups.push(group)
        }
      }
    }
    return Promise.resolve(this.compendiumCareerGroups)
  }

  public static getTrappingCategories(): string[] {
    return Object.keys(wfrp4e().config.trappingCategories)
  }

  public static async getCompendiumTrappings() {
    if (this.compendiumTrappings == null) {
      this.compendiumTrappings = []
      const trappingCategories = CompendiumUtil.getTrappingCategories()
      this.compendiumTrappings.push(
        ...((await this.getCompendiumItems()).filter((t: FoundryItem) => {
          const type = t?.system?.trappingType?.value
          return (
            trappingCategories.includes(t.type) ||
            trappingCategories.includes(type)
          )
        }) as FoundryTrapping[]),
      )
    }
    return Promise.resolve(this.compendiumTrappings)
  }

  public static async getCompendiumBestiary() {
    if (this.compendiumBestiary == null) {
      this.compendiumBestiary = {}

      const actorsMap = await this.getCompendiumActors()

      for (let [key, actors] of Object.entries(actorsMap)) {
        const creatures = actors.filter((c) => c.type === 'creature')
        if (creatures.length > 0) {
          this.compendiumBestiary[key] = creatures
        }
      }
    }
    return Promise.resolve(this.compendiumBestiary)
  }

  public static async getCompendiumSkills() {
    if (this.compendiumSkills == null) {
      this.compendiumSkills = []
      this.compendiumSkills.push(
        ...(await this.getCompendiumItems()).filter(
          (c: FoundryItem) => c.type === 'skill',
        ),
      )
    }
    return Promise.resolve(this.compendiumSkills)
  }

  public static async getCompendiumTalents() {
    if (this.compendiumTalents == null) {
      this.compendiumTalents = []
      this.compendiumTalents.push(
        ...(await this.getCompendiumItems()).filter(
          (c: FoundryItem) => c.type === 'talent',
        ),
      )
    }
    return Promise.resolve(this.compendiumTalents)
  }

  public static async getCompendiumTraits() {
    if (this.compendiumTraits == null) {
      this.compendiumTraits = []
      this.compendiumTraits.push(
        ...(await this.getCompendiumItems()).filter(
          (c: FoundryItem) => c.type === 'trait',
        ),
      )
    }
    return Promise.resolve(this.compendiumTraits)
  }

  public static async getCompendiumPsychologies() {
    if (this.compendiumPsychologies == null) {
      this.compendiumPsychologies = []
      this.compendiumPsychologies.push(
        ...(await this.getCompendiumItems()).filter(
          (c: FoundryItem) => c.type === 'psychology',
        ),
      )
    }
    return Promise.resolve(this.compendiumPsychologies)
  }

  public static async getCompendiumSizeTrait() {
    if (this.compendiumSizeTrait == null) {
      this.compendiumSizeTrait = <FoundryTrait>(
        (await this.getCompendiumTraits()).find(
          (t: FoundryTrait) =>
            t.name === i18n().localize('WFRP4NPCGEN.TRAIT.Size') ||
            t.flags?.babele?.originalName === 'Size',
        )
      )
    }
    return Promise.resolve(this.compendiumSizeTrait)
  }

  public static async getCompendiumSwarmTrait() {
    if (this.compendiumSwarmTrait == null) {
      this.compendiumSwarmTrait = <FoundryTrait>(
        (await this.getCompendiumTraits()).find(
          (t: FoundryTrait) =>
            t.name === i18n().localize('WFRP4NPCGEN.TRAIT.Swarm') ||
            t.flags?.babele?.originalName === 'Swarm',
        )
      )
    }
    return Promise.resolve(this.compendiumSwarmTrait)
  }

  public static async getCompendiumWeaponTrait() {
    if (this.compendiumWeaponTrait == null) {
      this.compendiumWeaponTrait = <FoundryTrait>(
        (await this.getCompendiumTraits()).find(
          (t: FoundryTrait) =>
            t.name === i18n().localize('WFRP4NPCGEN.TRAIT.Weapon') ||
            t.flags?.babele?.originalName === 'Weapon',
        )
      )
    }
    return Promise.resolve(this.compendiumWeaponTrait)
  }

  public static async getCompendiumArmourTrait() {
    if (this.compendiumArmorTrait == null) {
      this.compendiumArmorTrait = <FoundryTrait>(
        (await this.getCompendiumTraits()).find(
          (t: FoundryTrait) =>
            t.name === i18n().localize('WFRP4NPCGEN.TRAIT.Armour') ||
            t.flags?.babele?.originalName === 'Armour',
        )
      )
    }
    return Promise.resolve(this.compendiumArmorTrait)
  }

  public static async getCompendiumRangedTrait() {
    if (this.compendiumRangedTrait == null) {
      this.compendiumRangedTrait = <FoundryTrait>(
        (await this.getCompendiumTraits()).find(
          (t: FoundryTrait) =>
            t.name.startsWith(i18n().localize('WFRP4NPCGEN.TRAIT.Ranged')) ||
            t.flags?.babele?.originalName?.startsWith('Ranged'),
        )
      )
    }
    return Promise.resolve(this.compendiumRangedTrait)
  }

  public static getSizes(): { [key: string]: string } {
    return wfrp4e().config.actorSizes
  }

  public static async getCompendiumSpells() {
    if (this.compendiumSpells == null) {
      this.compendiumSpells = []
      this.compendiumSpells.push(
        ...((await this.getCompendiumItems()).filter(
          (c: FoundryItem) => c.type === 'spell',
        ) as FoundrySpell[]),
      )
    }
    return Promise.resolve(this.compendiumSpells)
  }

  public static async getCompendiumPrayers() {
    if (this.compendiumPrayers == null) {
      this.compendiumPrayers = []
      this.compendiumPrayers.push(
        ...((await this.getCompendiumItems()).filter(
          (c: FoundryItem) => c.type === 'prayer',
        ) as FoundryPrayer[]),
      )
    }
    return Promise.resolve(this.compendiumPrayers)
  }

  public static async getCompendiumPhysicalMutations() {
    if (this.compendiumPhysicalMutations == null) {
      this.compendiumPhysicalMutations = []
      this.compendiumPhysicalMutations.push(
        ...(await this.getCompendiumItems()).filter(
          (c: FoundryItem) =>
            c.type === 'mutation' &&
            c?.system?.mutationType.value === 'physical',
        ),
      )
    }
    return Promise.resolve(this.compendiumPhysicalMutations)
  }

  public static async getCompendiumMentalMutations() {
    if (this.compendiumMentalMutations == null) {
      this.compendiumMentalMutations = []
      this.compendiumMentalMutations.push(
        ...(await this.getCompendiumItems()).filter(
          (c: FoundryItem) =>
            c.type === 'mutation' && c?.system?.mutationType.value === 'mental',
        ),
      )
    }
    return Promise.resolve(this.compendiumMentalMutations)
  }

  private static loadCompendiumDocuments<T>(pack: FoundryPack): Promise<T> {
    return new Promise(async (resolve) => {
      try {
        const docs = (await pack?.getDocuments<T>()) ?? <T>[]
        resolve(docs)
      } catch (e) {
        console.error(e)
        const title = pack.title
        const collection = pack.collection
        const message = i18n().format(
          'WFRP4NPCGEN.notification.compendium.load.error',
          {
            title: title,
            collection: collection,
          },
        )
        console.warn(message)
        notifications().warn(message)
        resolve(<T>[])
      }
    })
  }
}
