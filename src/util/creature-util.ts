import { CreatureModel } from '../models/creature/creature-model'
import CreatureTemplate from '../models/creature/creature-template'
import CreatureAbilities from '../models/creature/creature-abilities'
import EntityUtil from './entity-util'
import StringUtil from './string-util'
import ReferentialUtil from './referential-util'
import CompendiumUtil from './compendium-util'
import RandomUtil from './random-util'
import { FoundryActor } from '../types/foundry/actor'
import { FoundryTrait } from '../types/foundry/trait'
import { FoundrySpell } from '../types/foundry/spell'
import { FoundryPrayer } from '../types/foundry/prayer'
import { FoundryMutation } from '../types/foundry/mutation'

export class CreatureUtil {
  public static async initCreatureModelFromTemlate(
    creature: FoundryActor,
    model: CreatureModel,
  ) {
    const creatureData = creature
    model.creature = creatureData
    model.template = new CreatureTemplate()
    model.abilities = new CreatureAbilities()

    const swarm = await CompendiumUtil.getCompendiumSwarmTrait()
    const weapon = await CompendiumUtil.getCompendiumWeaponTrait()
    const armour = await CompendiumUtil.getCompendiumArmourTrait()
    const ranged = await CompendiumUtil.getCompendiumRangedTrait()
    const size = await CompendiumUtil.getCompendiumSizeTrait()

    model.template.size = creatureData?.system?.details?.size?.value as string
    model.template.swarm = duplicate(creature.itemTypes?.trait)?.find(
      (t: FoundryTrait) => EntityUtil.match(t, swarm),
    ) as FoundryTrait
    model.template.isSwarm =
      model.template.swarm != null &&
      !model.template.swarm?.system?.disabled

    model.template.weapon = duplicate(creature.itemTypes?.trait)?.find(
      (t: FoundryTrait) => EntityUtil.match(t, weapon),
    ) as FoundryTrait
    model.template.hasWeaponTrait =
      model.template.weapon != null &&
      !model.template.weapon?.system?.disabled

    model.template.armour = duplicate(creature.itemTypes?.trait)?.find(
      (t: FoundryTrait) => EntityUtil.match(t, armour),
    ) as FoundryTrait
    model.template.hasArmourTrait =
      model.template.armour != null &&
      !model.template.armour?.system?.disabled

    model.template.ranged = duplicate(creature.itemTypes?.trait)?.find(
      (t: FoundryTrait) => EntityUtil.match(t, ranged),
    ) as FoundryTrait

    if (model.template.armour != null) {
      model.template.armourValue = model.template.armour.system?.specification
        .value as string
    }

    if (model.template.weapon != null) {
      model.template.weaponDamage = model.template.weapon.system?.specification
        .value as string
    }

    if (model.template.ranged != null) {
      model.template.rangedRange = StringUtil.getGroupName(
        model.template.ranged.name,
      )
      model.template.rangedDamage = model.template.ranged.system?.specification
        .value as string
      if (model.template.rangedDamage?.includes('+')) {
        model.template.rangedDamage = model.template.rangedDamage.replace(
          '+',
          '',
        )
      }
    }

    model.abilities.includeBasicSkills =
      (creature?.itemTypes?.skill?.filter(
        (s) => s?.system?.advanced?.value === 'bsc',
      )?.length ?? 0) > 0
    model.abilities.sizeKey = creatureData.system?.details?.size?.value ?? ''
    model.abilities.isSwarm = model.template.isSwarm
    model.abilities.hasWeaponTrait = model.template.hasWeaponTrait
    model.abilities.hasArmourTrait = model.template.hasArmourTrait
    model.abilities.hasRangedTrait =
      model.template.ranged != null &&
      !model.template.ranged?.system?.disabled
    model.abilities.weaponDamage = model.template.weaponDamage
    model.abilities.rangedRange = model.template.rangedRange
    model.abilities.rangedDamage = model.template.rangedDamage
    model.abilities.armourValue = model.template.armourValue

    const traits = creature.itemTypes?.trait?.filter((t: FoundryTrait) => {
      return (
        !EntityUtil.match(t, swarm) &&
        !EntityUtil.match(t, weapon) &&
        !EntityUtil.match(t, armour) &&
        !EntityUtil.match(t, ranged) &&
        !EntityUtil.match(t, size)
      )
    })
    const compendiumTraits = await ReferentialUtil.getTraitEntities(true)
    for (let trait of traits ?? []) {
      const compTrait = compendiumTraits.find(
        (t) =>
          t.name === trait.name ||
          t.name === trait.DisplayName ||
          t?.flags?.babele?.originalName === trait.name ||
          t?.flags?.babele?.originalName === trait.DisplayName,
      )
      const finalTrait = duplicate(compTrait ?? trait)
      finalTrait.DisplayName = trait.DisplayName ?? trait.name
      finalTrait.system.specification.value = trait.system.specification.value
      finalTrait._id = RandomUtil.getRandomId()
      finalTrait.pack = compTrait != null ? compTrait.pack : trait.pack
      finalTrait.system.disabled = trait.system.disabled
      model.abilities.traits.push(finalTrait)
    }

    const skills = creature.itemTypes?.skill?.filter((s) => {
      return s.system.advances.value > 0
    })
    const compendiumSkills = await ReferentialUtil.getSkillEntities(true)
    for (let skill of skills ?? []) {
      const compSkill = compendiumSkills.find(
        (s) =>
          s.name === skill.name || s.flags?.babele?.originalName === skill.name,
      )
      const finalSkill = duplicate(compSkill ?? skill)
      finalSkill._id = RandomUtil.getRandomId()
      finalSkill.pack = compSkill != null ? compSkill.pack : skill.pack
      finalSkill.system.advances.value = skill.system.advances.value
      model.abilities.skills.push(finalSkill)
    }

    const talents = creature.itemTypes?.talent
    const compendiumTalents = await ReferentialUtil.getTalentEntities(true)
    for (let talent of talents ?? []) {
      const compTal = compendiumTalents.find(
        (t) =>
          t.name === talent.name ||
          t.flags?.babele?.originalName === talent.name,
      )
      const finalTalent = duplicate(compTal ?? talent)
      finalTalent._id = RandomUtil.getRandomId()
      finalTalent.pack = compTal != null ? compTal.pack : talent.pack
      finalTalent.system.advances.value = talent.system.advances.value
      model.abilities.talents.push(finalTalent)
    }

    const allInventory = creature.itemTags
      ? [
          ...(creature.itemTags['weapon'] ?? []),
          ...(creature.itemTags['armour'] ?? []),
          ...(creature.itemTags['ammunition'] ?? []),
          ...(creature.itemTags['trapping'] ?? []),
          ...(creature.itemTags['cargo'] ?? []),
          ...(creature.itemTags['money'] ?? []),
          ...(creature.itemTags['container'] ?? []),
        ]
      : []
    model.trappings.push(...allInventory)

    const spells = await ReferentialUtil.getSpellEntities()
    const prayers = await ReferentialUtil.getPrayerEntities()
    const physicals = await ReferentialUtil.getPhysicalMutationEntities()
    const mentals = await ReferentialUtil.getMentalMutationEntities()

    model.spells = [
      ...(creature.itemTypes?.spell?.map((p: FoundrySpell) => {
        const spell = spells.find((s) => s.name === p.name)
        return spell != null ? duplicate(spell) : p
      }) ?? []),
    ]
    model.prayers = [
      ...(creature.itemTypes?.prayer?.map((b: FoundryPrayer) => {
        const prayer = prayers.find((p) => p.name === b.name)
        return prayer != null ? duplicate(prayer) : b
      }) ?? []),
    ]
    model.physicalMutations =
      creature.itemTypes?.mutation
        ?.filter(
          (m: FoundryMutation) => m.system?.mutationType.value === 'physical',
        )
        .map((m: FoundryMutation) => {
          const mutation = physicals.find((p) => p.name === m.name)
          return mutation != null ? duplicate(mutation) : m
        }) ?? []
    model.mentalMutations =
      creature.itemTypes?.mutation
        ?.filter(
          (m: FoundryMutation) => m.system?.mutationType.value === 'mental',
        )
        .map((m: FoundryMutation) => {
          const mutation = mentals.find((p) => p.name === m.name)
          return mutation != null ? duplicate(mutation) : m
        }) ?? []

    const others = creature.itemTags
      ? [
          ...(creature.itemTags['critical'] ?? []),
          ...(creature.itemTags['disease'] ?? []),
          ...(creature.itemTags['injury'] ?? []),
          ...(creature.itemTags['psychology'] ?? []),
        ]
      : []
    model.others.push(...others)
  }
}
