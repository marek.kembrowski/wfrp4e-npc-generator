import { folders } from '../constant'
import { FoundryFolder } from '../types/foundry/folder'

export default class FolderUtil {
  public static getFolderPath(id: string): string {
    const folder = folders().find((f: FoundryFolder) => f.uuid === id)
    if (folder == null) {
      return ''
    }

    let path: string = folder.name
    for (const ancestor of folder.ancestors) {
      path = `${ancestor.name}/${path}`
    }
    return path
  }

  public static async createNamedFolder(
    name: string,
    parent: FoundryFolder | null = null,
  ): Promise<FoundryFolder | null> {
    if (name == null || name.length === 0) {
      return null
    }
    let folder: FoundryFolder | null = null
    if (name.includes('/')) {
      let splitedName = name.split('/')
      if (splitedName.length > 3) {
        splitedName = splitedName.filter((_n, index) => index <= 3)
        console.warn(
          `Folder's name trunked to keep only 2 sub-folders maximum : ${splitedName.join(
            '/',
          )}`,
        )
      }
      for (let path of splitedName) {
        folder = await FolderUtil.createNamedFolder(path, folder)
      }
    } else {
      folder =
        folders().find(
          (f) =>
            f.type === 'Actor' &&
            f.name === name &&
            (f.ancestors[0]?._id ?? null) === (parent?._id ?? null),
        ) ??
        (await Folder.create({
          name: name,
          type: 'Actor',
          folder: parent,
        }))
    }
    return folder
  }
}
