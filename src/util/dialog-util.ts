import { i18n } from '../constant'

export default class DialogUtil {
  public static getLabelScript(
    label: string,
    style: string = null as any,
  ): string {
    const styleStr = style != null ? ` style="${style}"` : ''
    return `
        <label${styleStr}>
            ${i18n().localize(label)}          
        </label> 
        `
  }
}
