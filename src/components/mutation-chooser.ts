import { AbstractChooser } from './abstract-chooser'
import { SourcedModel } from '../models/common/sourced-model'
import { SourceSelectModel } from '../models/common/source-select-model'
import { i18n } from '../constant'
import RegisterSettings from '../util/register-settings'
import ChooserUtil from '../util/chooser-util'
import { GroupedSourceSelectModel } from '../models/common/grouped-source-select-model'
import { FoundryMutation } from '../types/foundry/mutation'

class Model {
  public physicalMutations: FoundryMutation[] = []
  public mentalMutations: FoundryMutation[] = []

  constructor(
    physicalMutations: FoundryMutation[],
    mentalMutations: FoundryMutation[],
  ) {
    this.physicalMutations = physicalMutations
    this.mentalMutations = mentalMutations
  }
}

export class MutationChooser extends AbstractChooser<
  Model,
  {
    mutationsTemplate: () => string
    mutationTemplate: () => string
    physicalMutations: GroupedSourceSelectModel[]
    mentalMutations: GroupedSourceSelectModel[]
    selectedPhysicalMutations: SourcedModel[]
    selectedMentalMutations: SourcedModel[]
  }
> {
  private selectedPhysicalMutation: SourceSelectModel | null = null
  private selectedMentalMutation: SourceSelectModel | null = null

  private callback: (
    physicalMutations: FoundryMutation[],
    mentalMutations: FoundryMutation[],
  ) => void

  constructor(
    object: Model,
    physicalMutations: GroupedSourceSelectModel[],
    mentalMutations: GroupedSourceSelectModel[],
    callback: (
      physicalMutations: FoundryMutation[],
      mentalMutations: FoundryMutation[],
    ) => void,
    previousCallback?: (() => void) | null,
    options?: Partial<FormApplicationOptions>,
  ) {
    super(object, previousCallback, options)
    this.model.mutationsTemplate = () =>
      `modules/${RegisterSettings.moduleName}/templates/mutation-rows.html`
    this.model.mutationTemplate = () =>
      `modules/${RegisterSettings.moduleName}/templates/mutation-row.html`

    this.callback = callback
    this.model.physicalMutations = physicalMutations
    this.model.mentalMutations = mentalMutations

    if (
      physicalMutations?.length > 0 &&
      physicalMutations[0].items.length > 0
    ) {
      this.selectedPhysicalMutation = physicalMutations[0].items[0]
    }
    if (mentalMutations?.length > 0 && mentalMutations[0].items.length > 0) {
      this.selectedMentalMutation = mentalMutations[0].items[0]
    }

    this.initSelectedFromModel()
  }

  public static get defaultOptions(): FormApplicationOptions {
    return mergeObject(super.defaultOptions, {
      id: 'mutation-chooser',
      title: i18n().localize('WFRP4NPCGEN.select.mutations.title'),
      template: `modules/${RegisterSettings.moduleName}/templates/mutation-chooser.html`,
      width: 550,
    })
  }

  public activateListeners(html: JQuery) {
    super.activateListeners(html)

    this.handleChange(html, '#mutations-select-physical', (event) => {
      const group =
        event.currentTarget.selectedOptions?.length > 0
          ? event.currentTarget.selectedOptions[0].getAttribute('data-group')
          : ''
      const key = event.currentTarget.value
      const groupedPhysical = this.model.physicalMutations.find(
        (gp) => gp.group === group,
      )
      this.selectedPhysicalMutation =
        groupedPhysical?.items?.find((p) => p.key === key) ?? null

      this.selectMutation(
        this.selectedPhysicalMutation,
        this.model.physicalMutations,
      )
    })
    this.handleChange(html, '#mutations-select-mental', (event) => {
      const group =
        event.currentTarget.selectedOptions?.length > 0
          ? event.currentTarget.selectedOptions[0].getAttribute('data-group')
          : ''
      const key = event.currentTarget.value
      const groupedMental = this.model.mentalMutations.find(
        (gp) => gp.group === group,
      )
      this.selectedMentalMutation =
        groupedMental?.items?.find((p) => p.key === key) ?? null
      this.selectMutation(
        this.selectedMentalMutation,
        this.model.mentalMutations,
      )
    })

    this.handleClick(html, '#mutations-add-physical', () => {
      this.addMutation(
        this.selectedPhysicalMutation,
        this.model.selectedPhysicalMutations,
        this.model.data.physicalMutations,
      )
      this.render()
    })
    this.handleClick(html, '#mutations-add-mental', () => {
      this.addMutation(
        this.selectedMentalMutation,
        this.model.selectedMentalMutations,
        this.model.data.mentalMutations,
      )
      this.render()
    })

    this.handleClick(html, '.mutation-remove-physical', (event) => {
      const key = event.currentTarget.value
      this.removeMutation(
        key,
        this.model.selectedPhysicalMutations,
        this.model.data.physicalMutations,
      )
      this.render()
    })
    this.handleClick(html, '.mutation-remove-mental', (event) => {
      const key = event.currentTarget.value
      this.removeMutation(
        key,
        this.model.selectedMentalMutations,
        this.model.data.mentalMutations,
      )
      this.render()
    })
  }

  protected isValid(_data: Model): boolean {
    return true
  }

  protected yes(data: Model) {
    this.callback(data?.physicalMutations, data?.mentalMutations)
  }

  public static async editMutations(
    initPhysicalMutations: FoundryMutation[],
    initMentalMutations: FoundryMutation[],
    callback: (
      physicalMutations: FoundryMutation[],
      mentalMutations: FoundryMutation[],
    ) => void,
    undo?: () => void,
  ) {
    const model = new Model(
      initPhysicalMutations ?? [],
      initMentalMutations ?? [],
    )

    const physicalMutations: GroupedSourceSelectModel[] =
      await ChooserUtil.getEditPhysicalMutations()
    const mentalMutations: GroupedSourceSelectModel[] =
      await ChooserUtil.getEditMentalMutations()

    new MutationChooser(
      model,
      physicalMutations,
      mentalMutations,
      callback,
      undo,
    ).render(true)
  }

  public initSelectedFromModel() {
    this.model.selectedPhysicalMutations =
      this.model.data.physicalMutations?.map(
        (s) => new SourcedModel(s, s.displayName ?? s.name),
      ) ?? []
    this.model.selectedMentalMutations =
      this.model.data.mentalMutations?.map(
        (t) => new SourcedModel(t, t.displayName ?? t.name),
      ) ?? []
  }

  private selectMutation(
    mutation: SourceSelectModel | null,
    mutations: GroupedSourceSelectModel[],
  ) {
    mutations.forEach((gm) => {
      gm.items.forEach((m) => {
        m.selected = mutation?.key === m.key
      })
    })
  }

  private addMutation(
    mutation: SourceSelectModel | null,
    selectedMutations: SourcedModel[],
    mutations: FoundryMutation[],
  ) {
    if (mutation != null) {
      const mutationData = mutation.source as FoundryMutation

      if (selectedMutations.find((a) => a.id === mutation.key) == null) {
        selectedMutations.push(new SourcedModel(mutationData, mutation.value))
        mutations.push(mutationData)
      }
    }
  }

  private removeMutation(
    id: string,
    selectedMutations: SourcedModel[],
    mutations: FoundryMutation[],
  ) {
    if (id?.length > 0) {
      let indexOfSelected = selectedMutations.findIndex((a) => a.id === id)
      if (indexOfSelected >= 0) {
        selectedMutations.splice(indexOfSelected, 1)
      }
      indexOfSelected = mutations.findIndex((a) => a._id === id)
      if (indexOfSelected >= 0) {
        mutations.splice(indexOfSelected, 1)
      }
    }
  }
}
