import { AbstractChooser } from './abstract-chooser'
import { GroupedSourceSelectModel } from '../models/common/grouped-source-select-model'
import { SourceSelectModel } from '../models/common/source-select-model'
import { i18n } from '../constant'
import RegisterSettings from '../util/register-settings'
import ChooserUtil from '../util/chooser-util'
import RandomUtil from '../util/random-util'
import { FoundryActor } from '../types/foundry/actor'
import { SelectModel } from '../models/common/select-model'

class Model {
  public creature: FoundryActor
  public generateAsPnj: boolean

  constructor(creature: FoundryActor, generateAsPnj: boolean) {
    this.creature = creature
    this.generateAsPnj = generateAsPnj
  }
}

export class CreatureChooser extends AbstractChooser<
  Model,
  {
    creatures: GroupedSourceSelectModel[]
    filteredCreatures: GroupedSourceSelectModel[]
    selectedImg: string
    actorGroups: SelectModel[]
  }
> {
  private selectedActor: SourceSelectModel | null
  private actorKeys: string[] = []

  private callback: (creature: FoundryActor, generateAsPnj: boolean) => void

  constructor(
    object: Model,
    creatures: GroupedSourceSelectModel[],
    callback: (creature: FoundryActor, generateAsPnj: boolean) => void,
    previousCallback?: (() => void) | null,
    options?: Partial<FormApplicationOptions>,
  ) {
    super(object, previousCallback, options)

    this.callback = callback
    this.model.creatures = creatures

    if (this.model.data.creature != null) {
      this.selectActor(this.model.data.creature.id ?? '')
    } else if (creatures?.length > 0 && creatures[0].items?.length > 0) {
      this.selectedActor = creatures[0].items[0]
      this.model.selectedImg = this.selectedActor?.source?.img as string
      this.model.data.creature = this.selectedActor.source as FoundryActor
    }

    this.initActorKeys(creatures)

    this.model.actorGroups = [new SelectModel('none', '', true)]
    this.model.actorGroups.push(
      ...(creatures?.map((gc) => {
        return new SelectModel(gc.group, gc.group, false)
      }) ?? []),
    )

    this.model.filteredCreatures = [...creatures]
  }

  public static get defaultOptions(): FormApplicationOptions {
    return mergeObject(super.defaultOptions, {
      id: 'creature-chooser',
      title: i18n().localize('WFRP4NPCGEN.creatures.select.title'),
      template: `modules/${RegisterSettings.moduleName}/templates/creature-chooser.html`,
      width: 550,
    })
  }

  public activateListeners(html: JQuery) {
    super.activateListeners(html)

    this.handleClick(html, '#randomActor', () => {
      const key = RandomUtil.getRandomValue(this.actorKeys)
      this.selectActor(key)
      this.render()
    })

    this.handleChange(html, '#selectPack', (event) => {
      const key = event.currentTarget.value
      this.selectPack(key)
      this.render()
    })

    this.handleChange(html, '#selectActor', (event) => {
      const key = event.currentTarget.value
      this.selectActor(key)
      this.render()
    })

    this.handleClick(html, '#generateAsPnj', () => {
      this.model.data.generateAsPnj = !this.model.data.generateAsPnj
      this.render()
    })
  }

  protected isValid(data: Model): boolean {
    return data?.creature != null
  }

  protected yes(data: Model) {
    this.callback(data?.creature, data?.generateAsPnj)
  }

  public static async selectCreature(
    initCreatureData: FoundryActor,
    generateAsPnj: boolean,
    callback: (creature: FoundryActor, generateAsPnj: boolean) => void,
    undo?: () => void,
  ) {
    const creatures: GroupedSourceSelectModel[] =
      await ChooserUtil.getCreatures()

    const initCreatureGroup = creatures.find(
      (gc) => gc.items.find((c) => c.key === initCreatureData?._id) != null,
    )
    const initCreature = initCreatureGroup?.items?.find(
      (c) => c.key === initCreatureData?._id,
    )

    const model = new Model(initCreature?.source as FoundryActor, generateAsPnj)

    new CreatureChooser(model, creatures, callback, undo).render(true)
  }

  private initActorKeys(creatures: GroupedSourceSelectModel[]) {
    this.actorKeys = []
    creatures.forEach((gc) => {
      gc.items.forEach((c) => {
        this.actorKeys.push(c.key)
      })
    })
  }

  private selectPack(key: string) {
    this.model.actorGroups.forEach((ag) => (ag.selected = key === ag.key))
    const creatureGroup = this.model.creatures.find((gt) => gt.group === key)
    let selectedActorKey: string
    if (creatureGroup) {
      this.model.filteredCreatures = [creatureGroup]
      selectedActorKey = creatureGroup.items[0].key
    } else {
      this.model.filteredCreatures = [...this.model.creatures]
      selectedActorKey = this.model.creatures[0].items[0].key
    }
    this.initActorKeys(this.model.filteredCreatures)
    this.selectActor(selectedActorKey)
  }

  private selectActor(key: string) {
    this.selectedActor = null
    this.model.selectedImg = ''
    this.model.creatures.forEach((gt) => {
      gt.items.forEach((a) => {
        const selected = a.key === key
        a.selected = selected
        if (selected) {
          this.selectedActor = a
          this.model.selectedImg = this.selectedActor.source?.img as string
          this.model.data.creature = this.selectedActor.source as FoundryActor
        }
        return selected
      })
    })
  }
}
