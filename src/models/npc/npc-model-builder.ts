import { ActorModelBuilder } from '../common/actor-model-builder'
import { NpcModel } from './npc-model'

export class NpcModelBuilder extends ActorModelBuilder<NpcModel> {
  constructor(model: NpcModel) {
    super(model)
  }
}
