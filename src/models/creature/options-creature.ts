import { IOptions } from '../common/options-int'
import { settings } from '../../constant'
import RegisterSettings from '../../util/register-settings'
import {
  GenerateEffectOptionEnum,
  getGenerateEffectOptionEnum,
} from '../../util/generate-effect-option.enum'

export class OptionsCreature implements IOptions {
  public withClassTrappings: boolean = false
  public withCareerTrappings: boolean = false
  public generateMoneyEffect: GenerateEffectOptionEnum =
    getGenerateEffectOptionEnum(
      settings().get(
        RegisterSettings.moduleName,
        'defaultCreatureGenerateMoneyEffect',
      ),
    )
  public generateWeaponEffect: GenerateEffectOptionEnum =
    GenerateEffectOptionEnum.NONE

  public withGenPathCareerName = false

  public withLinkedToken = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultCreatureWithLinkedToken',
  )

  public withInitialMoney = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultCreatureWithInitialMoney',
  )

  public withInitialWeapons = false

  public genPath: string = settings().get(
    RegisterSettings.moduleName,
    'defaultCreatureGenPath',
  )

  public imagePath: string | null = null

  public tokenPath: string | null = null

  public editAbilities: boolean = false

  public editTrappings = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultCreatureEditTrappings',
  )

  public addMagics = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultCreatureAddMagics',
  )

  public addMutations = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultCreatureAddMutations',
  )

  public noRandomCaras = settings().get<boolean>(
    RegisterSettings.moduleName,
    'defaultCreatureNoRandomCarac',
  )

  public noSkillAdvances = false
}
