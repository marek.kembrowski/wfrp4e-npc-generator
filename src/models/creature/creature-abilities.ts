import { FoundryTrait } from '../../types/foundry/trait'
import { FoundryTalent } from '../../types/foundry/talent'
import { FoundrySkill } from '../../types/foundry/skill'

export default class CreatureAbilities {
  public includeBasicSkills: boolean = false
  public sizeKey: string = 'avg'
  public isSwarm: boolean = false
  public hasWeaponTrait: boolean = false
  public hasRangedTrait: boolean = false
  public hasArmourTrait: boolean = false
  public weaponDamage: string
  public rangedRange: string
  public rangedDamage: string
  public armourValue: string
  public speciesKey: string = 'none'

  public traits: FoundryTrait[] = []
  public talents: FoundryTalent[] = []
  public skills: FoundrySkill[] = []
}
