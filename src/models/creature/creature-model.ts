import { ActorModel } from '../common/actor-model'
import CreatureAbilities from './creature-abilities'
import CreatureTemplate from './creature-template'
import { OptionsCreature } from './options-creature'
import { CreatureModelBuilder } from './creature-model-builder'
import { FoundryActor } from '../../types/foundry/actor'
import { FoundryTrapping } from '../../types/foundry/trapping'
import { FoundrySpell } from '../../types/foundry/spell'
import { FoundryPrayer } from '../../types/foundry/prayer'
import { FoundryMutation } from '../../types/foundry/mutation'
import { FoundryItem } from '../../types/foundry/item'

export class CreatureModel extends ActorModel {
  constructor() {
    super()
    this.options = new OptionsCreature()
  }

  public creature: FoundryActor
  public abilities: CreatureAbilities
  public template: CreatureTemplate

  public trappings: FoundryTrapping[] = []
  public spells: FoundrySpell[] = []
  public prayers: FoundryPrayer[] = []
  public physicalMutations: FoundryMutation[] = []
  public mentalMutations: FoundryMutation[] = []
  public others: FoundryItem[] = []

  public generateAsPnj: boolean = false

  public speciesKey: string = 'none'

  public nativeTongue: string | null

  public fromBuilder: CreatureModelBuilder | undefined = undefined
}
