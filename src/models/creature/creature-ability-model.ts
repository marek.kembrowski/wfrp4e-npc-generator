import { AbilityModel } from '../common/ability-model'
import { FoundryItem } from '../../types/foundry/item'

export class CreatureAbilityModel extends AbilityModel {
  constructor(source: FoundryItem, name: string, include: boolean) {
    super(source, name)
    this.include = include
  }

  public include: boolean
}
