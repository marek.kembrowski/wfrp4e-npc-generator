import { FoundryTrait } from '../../types/foundry/trait'

export default class CreatureTemplate {
  public size: string
  public swarm: FoundryTrait
  public weapon: FoundryTrait
  public ranged: FoundryTrait
  public armour: FoundryTrait
  public isSwarm: boolean = false
  public hasWeaponTrait: boolean = false
  public hasArmourTrait: boolean = false
  public weaponDamage: string
  public rangedRange: string
  public rangedDamage: string
  public armourValue: string
}
