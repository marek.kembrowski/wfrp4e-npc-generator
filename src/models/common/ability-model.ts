import StringUtil from '../../util/string-util'
import EntityUtil from '../../util/entity-util'
import { SourcedModel } from './sourced-model'
import { FoundryItem } from '../../types/foundry/item'

export class AbilityModel extends SourcedModel {
  constructor(source: FoundryItem, name: string) {
    super(source, name)
    this.simpleName = StringUtil.getSimpleName(name)
    this.hasGroupName = EntityUtil.hasGroup(source)
    this.groupName = this.hasGroupName ? StringUtil.getGroupName(name) : ''
    this.advance = source.system?.advances?.value ?? 0
    this.hasAdvances = source.system?.advances?.value != null
    this.specification = (source.system?.specification?.value as string) ?? ''
    this.hasSpecification = this.specification?.length > 0
  }

  public source: FoundryItem
  public id: string
  public name: string
  public simpleName: string
  public hasAdvances: boolean
  public hasGroupName: boolean
  public hasSpecification: boolean
  public groupName: string
  public advance: number
  public specification: string
}
