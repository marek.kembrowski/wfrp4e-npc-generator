import { SelectModel } from './select-model'
import EntityUtil from '../../util/entity-util'
import { FoundryItem } from '../../types/foundry/item'
import { FoundryActor } from '../../types/foundry/actor'

export class SourceSelectModel extends SelectModel {
  constructor(
    key: string,
    value: string,
    selected: boolean,
    source: FoundryItem | FoundryActor,
  ) {
    super(key, value, selected)
    this.source = source
    this.isWorldItem = EntityUtil.isWorldItem(source)
  }

  public source: FoundryItem | FoundryActor
  public isWorldItem: boolean
}
