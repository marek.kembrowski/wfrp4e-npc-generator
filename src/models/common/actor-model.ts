import { IOptions } from './options-int'

export abstract class ActorModel {
  public name: string
  public options: IOptions
  public cityBorn: string | null
  public careers: string[] = []
}
