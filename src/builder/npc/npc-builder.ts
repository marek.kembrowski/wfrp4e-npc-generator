import { NpcModel } from '../../models/npc/npc-model'
import WaiterUtil from '../../util/waiter-util'
import { NpcModelBuilder } from '../../models/npc/npc-model-builder'
import ReferentialUtil from '../../util/referential-util'
import StringUtil from '../../util/string-util'
import {
  psychologyPrefix,
  traitPrefix,
} from '../../referential/species-referential'
import RandomUtil from '../../util/random-util'
import { CharModel } from '../../models/common/char-model'
import FolderUtil from '../../util/folder-util'
import EntityUtil from '../../util/entity-util'
import TrappingUtil from '../../util/trapping-util'
import { GenerateEffectOptionEnum } from '../../util/generate-effect-option.enum'
import { ActorBuilder } from '../common/actor-builder'
import { FoundryActor, FoundryPrototypeToken } from '../../types/foundry/actor'

export class NpcBuilder extends ActorBuilder {
  public static readonly referential = ReferentialUtil

  public static async buildNpc(
    model: NpcModel,
    callback: (model: NpcModelBuilder) => void,
  ) {
    const modelBuilder = model.fromBuilder ?? new NpcModelBuilder(model)
    await WaiterUtil.show(
      'WFRP4NPCGEN.npc.generation.inprogress.title',
      'WFRP4NPCGEN.npc.generation.inprogress.hint',
      async () => {
        console.info('Prepare Species Value')
        await this.addSpeciesValue(modelBuilder)

        console.info('Prepare Career Path')
        await this.addCareerPath(modelBuilder)

        console.info('Prepare Status')
        await this.addStatus(modelBuilder)

        console.info('Prepare Basic skills')
        await this.addBasicSkill(modelBuilder)

        console.info('Prepare Native Tongue')
        await this.addNativeTongueSkill(modelBuilder)

        console.info('Prepare City Born')
        await this.addCityBornSkill(modelBuilder)

        console.info('Prepare Career Skills')
        await this.addCareerSkill(modelBuilder)

        console.info('Prepare Species Skills')
        await this.addSpeciesSkill(modelBuilder)

        console.info('Prepare Species Talents')
        await this.addSpeciesTalents(modelBuilder)

        console.info('Prepare Career Talents')
        await this.addCareerTalents(modelBuilder)

        console.info('Prepare Species Traits')
        await this.addSpeciesTraits(modelBuilder)

        console.info('Prepare Species Psychologies')
        await this.addSpeciesPsychologies(modelBuilder)

        console.info('Prepare Basic Chars')
        await this.addBasicChars(modelBuilder)

        console.info('Prepare Movement')
        await this.addMovement(modelBuilder)

        if (!model.options.noSkillAdvances) {
          console.info('Prepare Skills Advances')
          await this.addAdvanceSkills(modelBuilder)

          console.info('Prepare Species Skills Advances')
          await this.addAdvanceSpeciesSkills(modelBuilder)

          console.info('Prepare Chars Advances')
          await this.addAdvanceChars(modelBuilder)
        }

        const trappings: string[] = []
        if (model.options.withClassTrappings) {
          console.info('Prepare Class Trappings')
          await this.prepareClassTrappings(modelBuilder, trappings)
        }
        if (model.options.withCareerTrappings) {
          console.info('Prepare Career Trappings')
          await this.prepareCareerTrappings(modelBuilder, trappings)
        }
        console.info('Prepare Trappings')
        await this.addTrappings(modelBuilder, trappings)

        await this.editAbilities<NpcModel>(modelBuilder, callback)
      },
    )
  }

  private static async addSpeciesValue(model: NpcModelBuilder) {
    const hasSubSpecies = model.model.subSpeciesKey?.length > 0
    const speciesMap = ReferentialUtil.getSpeciesMap()
    const subSpeciesMap =
      ReferentialUtil.getSpeciesSubSpeciesMap(model.model.speciesKey) ?? {}
    model.speciesValue =
      model.speciesValue ??
      (hasSubSpecies
        ? `${speciesMap[model.model.speciesKey]} (${
            subSpeciesMap[model.model.subSpeciesKey].name
          })`
        : speciesMap[model.model.speciesKey])
  }

  private static async addBasicSkill(model: NpcModelBuilder) {
    const skillNames = (await this.referential.getAllBasicSkills()).map(
      (s) => s.name,
    )
    await this.addSkills(model, skillNames)
  }

  private static async addNativeTongueSkill(model: NpcModelBuilder) {
    const nativeTongue =
      ReferentialUtil.getNativeTongue(
        model.model.speciesKey,
        model.model.subSpeciesKey,
      ) ?? ''
    if (nativeTongue?.length > 0) {
      await this.addSkill(model, nativeTongue)
    }
  }

  private static async addSpeciesSkill(model: NpcModelBuilder) {
    const speciesSkill = model.model.speciesSkills.majors.concat(
      model.model.speciesSkills.minors,
    )
    await this.addSkills(model, speciesSkill)
  }

  private static async addSpeciesTalents(model: NpcModelBuilder) {
    const speciesOthers = await this.getSpeciesOthers(model)
    const allTalents = speciesOthers.filter((t) => !t.startsWith('@'))

    await this.addTalents(model, allTalents)
  }

  private static async addSpeciesTraits(model: NpcModelBuilder) {
    const speciesOthers = await this.getSpeciesOthers(model)
    const allTraits = speciesOthers
      .filter((t) => {
        return t.startsWith(traitPrefix)
      })
      .map((t) => t.replace(traitPrefix, ''))

    await this.addTraits(model, allTraits)
  }

  private static async addSpeciesPsychologies(model: NpcModelBuilder) {
    const speciesOthers = await this.getSpeciesOthers(model)
    const allPsy = speciesOthers
      .filter((t) => {
        return t.startsWith(psychologyPrefix)
      })
      .map((t) => t.replace(psychologyPrefix, ''))

    await this.addPsychologies(model, allPsy)
  }

  private static async addBasicChars(model: NpcModelBuilder) {
    const averageChars = await this.referential.getSpeciesCharacteristics(
      model.model.speciesKey,
    )
    Object.entries(averageChars).forEach(([key, char]) => {
      const positive = RandomUtil.getRandomBoolean()
      const amplitude = RandomUtil.getRandomPositiveNumber(6)
      const adjust = model.model?.options?.noRandomCaras
        ? 0
        : (positive ? 1 : -1) * RandomUtil.getRandomPositiveNumber(amplitude)
      const charValue = char.value
      model.chars[key] = new CharModel(
        (model.chars[key]?.initial ?? charValue) + adjust,
        model.chars[key]?.advances ?? 0,
      )
    })
  }

  private static async addMovement(model: NpcModelBuilder) {
    model.move =
      model.move ??
      (await this.referential.getSpeciesMovement(model.model.speciesKey))
  }

  private static async addAdvanceSpeciesSkills(model: NpcModelBuilder) {
    model.model.speciesSkills.majors.forEach((skill) => {
      const sk = model.skills.find((s) => s.name === skill)
      if (sk != null && sk.system.advances.value === 0) {
        sk.system.advances.value += 5
      }
    })
    model.model.speciesSkills.minors.forEach((skill) => {
      const sk = model.skills.find((s) => s.name === skill)
      if (sk != null && sk.system.advances.value === 0) {
        sk.system.advances.value += 3
      }
    })
  }

  private static async addTraits(model: NpcModelBuilder, names: string[]) {
    if (names == null || names.length === 0) {
      return
    }
    for (let name of names) {
      await this.addTrait(model, name)
    }
  }

  private static async addTrait(model: NpcModelBuilder, name: string) {
    if (name == null || name.length === 0) {
      return
    }

    try {
      const traitToAdd = await this.referential.findTrait(name)
      if (
        !StringUtil.arrayIncludesDeburrIgnoreCase(
          model.traits.map((ms) => ms.name),
          traitToAdd.name,
        )
      ) {
        model.traits.push(traitToAdd)
      }
    } catch (e) {
      console.warn('Cant find Trait : ' + name)
    }
  }

  private static async addPsychologies(
    model: NpcModelBuilder,
    names: string[],
  ) {
    if (names == null || names.length === 0) {
      return
    }
    for (let name of names) {
      await this.addPsychology(model, name)
    }
  }

  private static async addPsychology(model: NpcModelBuilder, name: string) {
    if (name == null || name.length === 0) {
      return
    }

    try {
      const psyToAdd = await this.referential.findPsychology(name)
      if (
        !StringUtil.arrayIncludesDeburrIgnoreCase(
          model.psychologies.map((ms) => ms.name),
          psyToAdd.name,
        )
      ) {
        model.psychologies.push(psyToAdd)
      }
    } catch (e) {
      console.warn('Cant find Psychology : ' + name)
    }
  }

  private static async getSpeciesOthers(
    model: NpcModelBuilder,
  ): Promise<string[]> {
    const hasSubSpecies = model.model.subSpeciesKey?.length > 0
    const hasOrigin = model.model.speciesOthers.originKey?.length > 0
    const autoOthers = (
      hasSubSpecies
        ? await ReferentialUtil.getSubSpeciesOthers(
            model.model.speciesKey,
            model.model.subSpeciesKey,
            false,
            true,
          )
        : await ReferentialUtil.getSpeciesOthers(
            model.model.speciesKey,
            false,
            true,
          )
    ).map((t) => String(t))

    let originAutoOthers: string[] = []
    if (hasOrigin) {
      originAutoOthers = (
        hasSubSpecies
          ? await ReferentialUtil.getSubSpeciesOrigin(
              model.model.speciesKey,
              model.model.subSpeciesKey,
              model.model.speciesOthers.originKey,
              false,
              true,
            )
          : await ReferentialUtil.getSpeciesOrigin(
              model.model.speciesKey,
              model.model.speciesOthers.originKey,
              false,
              true,
            )
      ).map((t) => String(t))
    }

    const allOthers: string[] = [
      ...autoOthers,
      ...(model.model.speciesOthers?.others ?? []),
      ...(model.model.speciesOthers?.randomTalents ?? []),
      ...originAutoOthers,
      ...(model.model.speciesOthers?.origin ?? []),
    ]

    return Promise.resolve(allOthers)
  }

  public static async buildActorData(model: NpcModelBuilder, type: string) {
    const actorData: FoundryActor = {
      name: model.model.name,
      type: type,
      flags: {
        autoCalcRun: true,
        autoCalcWalk: true,
        autoCalcWounds: true,
        autoCalcCritW: true,
        autoCalcCorruption: true,
        autoCalcEnc: true,
        autoCalcSize: true,
      },
      system: {
        characteristics: model.chars,
        details: {
          move: {
            value: model.move,
          },
          species: {
            value: model.speciesValue,
          },
          status: {
            value: model.status,
          },
        },
      },
      items: [
        ...model.careers.map((c) => {
          const cd = duplicate(c)
          delete cd._id
          return cd
        }),
      ],
    }
    if (
      model.model.options?.imagePath != null &&
      model.model.options.imagePath.length > 0
    ) {
      actorData.img = model.model.options.imagePath
    }
    return Promise.resolve(actorData)
  }

  public static async createActor(model: NpcModelBuilder, data: FoundryActor) {
    const options = model?.model?.options
    if (options?.genPath?.length > 0 || options?.withGenPathCareerName) {
      const genPaths =
        options?.genPath?.length > 0
          ? options?.genPath.split('/').filter((p) => p != null && p.length > 0)
          : []
      if (options?.withGenPathCareerName) {
        const careerData = model.career?.system
        genPaths.push(careerData?.careergroup?.value as string)
      }
      if (genPaths.length > 0) {
        const folder = await FolderUtil.createNamedFolder(genPaths.join('/'))
        data.folder = folder?.id
      }
    }

    let actor = await Actor.create(data)
    if (actor.createEmbeddedDocuments) {
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toDuplicateRecords(model.skills),
          { skipSpecialisationChoice: true },
      )
      await actor.createEmbeddedDocuments(
        Item.metadata.name,
        EntityUtil.toDuplicateRecords(model.talents),
          { skipSpecialisationChoice: true },
      )
      if (model.traits.length > 0) {
        await actor.createEmbeddedDocuments(
          Item.metadata.name,
          EntityUtil.toDuplicateRecords(model.traits),
            { skipSpecialisationChoice: true },
        )
      }
      if (model.trappings.length > 0) {
        await actor.createEmbeddedDocuments(
          Item.metadata.name,
          EntityUtil.toDuplicateRecords(model.trappings),
        )
      }
      if (model.spells.length > 0) {
        await actor.createEmbeddedDocuments(
          Item.metadata.name,
          EntityUtil.toDuplicateRecords(model.spells),
        )
      }
      if (model.prayers.length > 0) {
        await actor.createEmbeddedDocuments(
          Item.metadata.name,
          EntityUtil.toDuplicateRecords(model.prayers),
        )
      }
      if (model.physicalMutations.length > 0) {
        await actor.createEmbeddedDocuments(
          Item.metadata.name,
          EntityUtil.toDuplicateRecords(model.physicalMutations),
        )
      }
      if (model.mentalMutations.length > 0) {
        await actor.createEmbeddedDocuments(
          Item.metadata.name,
          EntityUtil.toDuplicateRecords(model.mentalMutations),
        )
      }
      if (model.psychologies.length > 0) {
        await actor.createEmbeddedDocuments(
          Item.metadata.name,
          EntityUtil.toDuplicateRecords(model.psychologies),
        )
      }
    }

    if (actor.updateEmbeddedDocuments) {
      for (const career of actor.itemTypes?.career ?? []) {
        await actor.updateEmbeddedDocuments(Item.metadata.name, [
          {
            _id: career.id,
            'system.complete.value': true,
          },
        ])
      }
    }

    await TrappingUtil.initMoney(actor)

    if (options?.withInitialMoney) {
      await TrappingUtil.generateMoney(actor)
    }

    if (options?.withInitialWeapons) {
      await TrappingUtil.generateWeapons(actor)
    }

    if (
      !options?.withLinkedToken &&
      !options?.withInitialMoney &&
      GenerateEffectOptionEnum.NONE !== options.generateMoneyEffect
    ) {
      await this.addGenerateTokenEffect(
        actor,
        'WFRP4NPCGEN.trappings.money.label',
        GenerateEffectOptionEnum.DEFAULT_DISABLED ===
          options.generateMoneyEffect,
        'modules/wfrp4e-core/art/other/gold.webp',
      )
    }

    if (
      !options?.withLinkedToken &&
      !options?.withInitialWeapons &&
      GenerateEffectOptionEnum.NONE !== options.generateWeaponEffect
    ) {
      await this.addGenerateTokenEffect(
        actor,
        'WFRP4NPCGEN.trappings.weapon.label',
        GenerateEffectOptionEnum.DEFAULT_DISABLED ===
          options.generateWeaponEffect,
        'modules/wfrp4e-core/art/other/weapons.webp',
      )
    }

    const token = actor.prototypeToken
    const update: FoundryPrototypeToken = {}
    let performUpdate = false

    if (
      token != null &&
      options?.tokenPath != null &&
      options.tokenPath.length > 0
    ) {
      performUpdate = true
      update.texture = {
        src: options.tokenPath,
      }
      update.randomImg = options.tokenPath.includes('*')
    }

    if (token != null && options?.withLinkedToken) {
      performUpdate = true
      update.actorLink = options?.withLinkedToken
    }

    if (performUpdate && actor.update) {
      actor = await actor.update({
        prototypeToken: mergeObject(token, update, { inplace: false }),
      })
    }

    return Promise.resolve(actor)
  }
}
