export interface FoundryActiveEffect {
  label: string
  disabled: boolean
}

export interface FoundryActiveEffectCreateData {
  icon?: string
  label: string
  disabled: boolean
}
