import { FoundryTrait } from './trait'
import { FoundrySkill } from './skill'
import { FoundryTalent } from './talent'
import { FoundryItem } from './item'
import { FoundrySpell } from './spell'
import { FoundryPrayer } from './prayer'
import { FoundryMutation } from './mutation'
import { FoundryTrapping } from './trapping'
import {
  FoundryActiveEffect,
  FoundryActiveEffectCreateData,
} from './active-effect'
import { CharsModel } from '../../models/common/char-model'
import { FoundryCareer } from './career'
import { FoundryPsychology } from './psychology'

export interface FoundryActor {
  id?: string
  _id?: string
  name: string
  type: string
  img?: string
  folder?: string
  flags?: {
    autoCalcRun: boolean
    autoCalcWalk: boolean
    autoCalcWounds: boolean
    autoCalcCritW: boolean
    autoCalcCorruption: boolean
    autoCalcEnc: boolean
    autoCalcSize: boolean
  }
  document?: {
    pack: string
  }
  pack?: string
  itemTypes?: {
    trait: FoundryTrait[]
    skill: FoundrySkill[]
    talent: FoundryTalent[]
    spell: FoundrySpell[]
    prayer: FoundryPrayer[]
    mutation: FoundryMutation[]
    money: FoundryTrapping[]
    weapon: FoundryTrapping[]
    career: FoundryCareer[]
    psychology: FoundryPsychology[]
    trapping: FoundryTrapping[]
  }
  system: FoundryActorData
  itemTags?: {
    [key: string]: FoundryItem[]
  }
  updateEmbeddedDocuments?: (
    type: string,
    item:
      | [
          {
            _id: string
            [key: string]: number | string
          },
        ]
      | Record<string, unknown>[],
  ) => Promise<void>
  createEmbeddedDocuments?: (
    type: string,
    item: [FoundryActiveEffectCreateData] | Record<string, unknown>[],
    options?: { skipSpecialisationChoice: boolean },
  ) => Promise<void>
  deleteEmbeddedDocuments?: (type: string, ids: string[]) => Promise<void>
  effects?: FoundryActiveEffect[]
  items?: FoundryItem[]
  update?: (data: { [key: string]: object }) => Promise<FoundryActor>
  prototypeToken?: FoundryPrototypeToken
}

export interface FoundryActorData {
  details: {
    move?: {
      value: string
    }
    species?: {
      value: string
    }
    biography?: {
      value: string
    }
    gender?: {
      value: string
    }
    hitLocationTable?: {
      value: string
    }
    status?: {
      value: string
      standing?: number
      tier?: string
    }
    size?: {
      value: string
    }
  }
  characteristics: CharsModel
}

export interface FoundryToken {
  id: string
  parent: FoundryScene
  actorLink: boolean
}

export interface FoundryPrototypeToken {
  randomImg?: boolean
  actorLink?: boolean
  texture?: {
    src: string
  }
}

export interface FoundryScene {
  updateEmbeddedDocuments: (
    type: string,
    item: [FoundryActor] | Record<string, unknown>[],
  ) => Promise<void>
}
