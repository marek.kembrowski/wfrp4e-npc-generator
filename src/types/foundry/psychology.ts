import { FoundryItem, FoundryItemData } from './item'

export interface FoundryPsychology extends FoundryItem<FoundryPsychologyData> {}
export interface FoundryPsychologyData extends FoundryItemData {}
