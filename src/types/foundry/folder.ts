export interface FoundryFolder {
  id: string
  _id: string
  uuid: string
  name: string
  type: string
  ancestors: FoundryFolder[]
}
