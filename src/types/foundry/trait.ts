import { FoundryItem, FoundryItemData } from './item'

export interface FoundryTrait extends FoundryItem<FoundryTraitData> {}

export interface FoundryTraitData extends FoundryItemData {}
