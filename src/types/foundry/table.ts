export interface FoundryTable {
  flags?: {
    wfrp4e?: {
      column: string
      key: string
    }
  }
  results: [
    {
      text: string
      documentId: string
    },
  ]
}
