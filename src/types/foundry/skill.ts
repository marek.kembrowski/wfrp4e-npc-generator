import { FoundryItem, FoundryItemData } from './item'

export interface FoundrySkill extends FoundryItem<FoundrySkillData> {}

export interface FoundrySkillData extends FoundryItemData {}
