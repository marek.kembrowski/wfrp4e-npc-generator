import { FoundryItem, FoundryItemData } from './item'

export interface FoundryMutation extends FoundryItem<FoundryMutationData> {}
export interface FoundryMutationData extends FoundryItemData {}
