import { FoundryGame, FoundryUi } from './types/foundry/game'
import { FoundryItem } from './types/foundry/item'

export const rootGame: () => FoundryGame = () => game
export const rootUi: () => FoundryUi = () => ui
export const wfrp4e = () => rootGame().wfrp4e
export const user = () => rootGame().user
export const actors = () => rootGame().actors
export const i18n = () => rootGame().i18n
export const modules = () => rootGame().modules
export const packs = () => rootGame().packs
export const babele = () => rootGame().babele
export const folders = () => rootGame().folders
export const settings = () => rootGame().settings
export const items: <T extends FoundryItem = FoundryItem>() => T[] = <
  T extends FoundryItem = FoundryItem,
>() => rootGame().items as T[]
export const world = () => rootGame().world
export const notifications = () => rootUi().notifications
export const initTemplates = (paths: string[]) => loadTemplates(paths)
export const names = () => rootGame().wfrp4e.names
export const generateName = (speciesKey: string) =>
  names().generateName({ species: speciesKey })
export const isGM = () => user()?.isGM ?? false
export const tables = () => rootGame().tables
