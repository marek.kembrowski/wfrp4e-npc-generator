import { i18n, notifications } from '../../constant'
import WaiterUtil from '../../util/waiter-util'
import { CreatureModelBuilder } from '../../models/creature/creature-model-builder'
import ReferentialUtil from '../../util/referential-util'
import { CreatureBuilder } from '../../builder/creature/creature-builder'
import CheckDependencies from '../../check-dependencies'
import { CreatureModel } from '../../models/creature/creature-model'
import { CreatureChooser } from '../../components/creature-chooser'
import { CreatureUtil } from '../../util/creature-util'
import { CreatureAbilityChooser } from '../../components/creature-ability-chooser'
import CompendiumUtil from '../../util/compendium-util'
import { NameChooser } from '../../components/name-chooser'
import { OptionsChooser } from '../../components/options-chooser'
import { OptionsCreature } from '../../models/creature/options-creature'
import { OptionsNpc } from '../../models/npc/options-npc'
import { CareerChooser } from '../../components/career-chooser'
import { CreaturePnjChooser } from '../../components/creature-pnj-chooser'
import { IOptions } from '../../models/common/options-int'
import { FoundryActor } from '../../types/foundry/actor'

export class CreatureGenerator {
  public static readonly referential = ReferentialUtil

  public static async generateFromActor(
    actor: FoundryActor,
    callback?: (
      model: CreatureModelBuilder,
      actorData: FoundryActor,
      actor: FoundryActor,
    ) => void,
  ) {
    await this.generateCreature(callback, actor)
  }

  public static async generateCreature(
    callback?: (
      model: CreatureModelBuilder,
      actorData: FoundryActor,
      actor: FoundryActor,
    ) => void,
    fromActor: FoundryActor | undefined = undefined,
  ) {
    await this.referential.initReferential(async () => {
      await this.generateCreatureModel(async (model) => {
        const actorData = await CreatureBuilder.buildCreatureData(model)
        const actor = await CreatureBuilder.createCreature(model, actorData)
        notifications().info(
          i18n().format('WFRP4NPCGEN.notification.creature.created', {
            name: actor.name,
          }),
        )
        await WaiterUtil.hide()
        if (callback != null) {
          callback(model, actorData, actor)
        }
      }, fromActor)
    }, true)
  }

  public static async generateCreatureModel(
    callback: (model: CreatureModelBuilder) => void,
    fromActor: FoundryActor | undefined = undefined,
  ) {
    const creatureModel = new CreatureModel()
    CheckDependencies.check(async (canRun) => {
      if (canRun) {
        if (fromActor == null) {
          this.selectCreature(creatureModel, callback)
        } else {
          creatureModel.generateAsPnj = false
          creatureModel.options = new OptionsCreature()
          creatureModel.options.noRandomCaras = true
          creatureModel.options.withLinkedToken = true
          await CreatureUtil.initCreatureModelFromTemlate(
            fromActor,
            creatureModel,
          )
          this.selectCreatureAbilities(creatureModel, callback, true)
        }
      }
    })
  }

  private static async selectCreature(
    model: CreatureModel,
    callback: (model: CreatureModelBuilder) => void,
  ) {
    await CreatureChooser.selectCreature(
      model.creature,
      model.generateAsPnj,
      async (creature, generateAsPnj) => {
        model.generateAsPnj = generateAsPnj
        if (generateAsPnj) {
          model.options = new OptionsNpc()
        } else {
          model.options = new OptionsCreature()
        }
        await CreatureUtil.initCreatureModelFromTemlate(creature, model)

        if (generateAsPnj) {
          this.selectCreaturePnj(model, callback)
        } else {
          this.selectCreatureAbilities(model, callback)
        }
      },
    )
  }

  private static async selectCreatureAbilities(
    model: CreatureModel,
    callback: (model: CreatureModelBuilder) => void,
    fromActor = false,
  ) {
    await CreatureAbilityChooser.selectCreatureAbilities(
      model.abilities,
      (abilities) => {
        model.abilities = abilities
        this.selectName(model, callback)
      },
      fromActor
        ? undefined
        : () => {
            this.selectCreature(model, callback)
          },
    )
  }

  private static async selectCreaturePnj(
    model: CreatureModel,
    callback: (model: CreatureModelBuilder) => void,
  ) {
    await CreaturePnjChooser.selectSpecies(
      model.speciesKey,
      model.cityBorn,
      model.nativeTongue,
      (speciesKey, cityBorn, nativeTongue) => {
        model.speciesKey = speciesKey
        model.cityBorn = cityBorn
        model.nativeTongue = nativeTongue
        this.selectCareer(model, callback)
      },
      () => {
        this.selectCreature(model, callback)
      },
    )
  }

  private static async selectCareer(
    model: CreatureModel,
    callback: (model: CreatureModelBuilder) => void,
  ) {
    await CareerChooser.selectCareer(
      model.careers,
      model.speciesKey,
      null,
      false,
      (careers: string[]) => {
        model.careers = careers

        this.selectName(model, callback)
      },
      () => {
        this.selectCreaturePnj(model, callback)
      },
    )
  }

  private static async selectName(
    model: CreatureModel,
    callback: (model: CreatureModelBuilder) => void,
  ) {
    if (model.name == null || model.name.length === 0) {
      const swarm = await CompendiumUtil.getCompendiumSwarmTrait()
      const swarmLabel = model.abilities.isSwarm ? `, ${swarm.name}` : ''

      model.name = `${model.creature.name} (${
        CompendiumUtil.getSizes()[model.abilities.sizeKey]
      }${swarmLabel})`
    }
    await NameChooser.selectName(
      model.name,
      model.abilities.speciesKey,
      model.abilities.speciesKey != 'none',
      (name: string) => {
        model.name = name
        this.selectOptions(model, callback)
      },
      () => {
        model.name = ''
        if (model.generateAsPnj) {
          this.selectCareer(model, callback)
        } else {
          this.selectCreatureAbilities(model, callback)
        }
      },
    )
  }

  private static async selectOptions(
    model: CreatureModel,
    callback: (model: CreatureModelBuilder) => void,
  ) {
    model.options.imagePath = model.creature.img ?? ''
    model.options.tokenPath = model.creature.prototypeToken?.texture?.src ?? ''

    await OptionsChooser.selectOptions(
      !model.generateAsPnj,
      model.options,
      model.generateAsPnj && model.speciesKey !== 'none'
        ? model.speciesKey
        : 'creature',
      (options: IOptions) => {
        model.options = options
        CreatureBuilder.buildCreature(model, callback)
      },
      () => {
        this.selectName(model, callback)
      },
    )
  }
}
