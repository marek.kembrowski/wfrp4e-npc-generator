// import {i18n, notifications} from "../../constant.js";
import WaiterUtil from '../../util/waiter-util'
import CheckDependencies from '../../check-dependencies'
import { NpcModel } from '../../models/npc/npc-model'
import CompendiumUtil from '../../util/compendium-util'
import { SpeciesChooser } from '../../components/species-chooser'
import { CareerChooser } from '../../components/career-chooser'
import { SpeciesSkillsChooser } from '../../components/species-skills-chooser'
import ReferentialUtil from '../../util/referential-util'
import { SpeciesOthersChooser } from '../../components/species-others-chooser'
import { SpeciesOthers } from '../../models/npc/species-others'
import { SpeciesSkills } from '../../models/npc/species-skills'
import { NameChooser } from '../../components/name-chooser'
import { OptionsChooser } from '../../components/options-chooser'
import { NpcModelBuilder } from '../../models/npc/npc-model-builder'
import { NpcBuilder } from '../../builder/npc/npc-builder'
import { i18n, notifications } from '../../constant'
import { OptionsNpc } from '../../models/npc/options-npc'
import EntityUtil from '../../util/entity-util'
import { CharModel } from '../../models/common/char-model'
import { FoundryActor } from '../../types/foundry/actor'
import { FoundrySkill } from '../../types/foundry/skill'
import { FoundryTalent } from '../../types/foundry/talent'
import { FoundryTrait } from '../../types/foundry/trait'
import { FoundryPsychology } from '../../types/foundry/psychology'
import { FoundrySpell } from '../../types/foundry/spell'
import { FoundryPrayer } from '../../types/foundry/prayer'
import { FoundryMutation } from '../../types/foundry/mutation'
import { FoundryTrapping } from '../../types/foundry/trapping'

export class NpcGenerator {
  public static readonly compendium = CompendiumUtil
  public static readonly referential = ReferentialUtil
  public static readonly speciesChooser = SpeciesChooser
  public static readonly careerChooser = CareerChooser
  public static readonly speciesSkillsChooser = SpeciesSkillsChooser
  public static readonly speciesOthersChooser = SpeciesOthersChooser
  public static readonly nameChooser = NameChooser
  public static readonly optionsChooser = OptionsChooser

  public static async generateFromActor(
    actor: FoundryActor,
    callback?: (
      model: NpcModelBuilder,
      actorData: FoundryActor,
      actor: FoundryActor,
    ) => void,
  ) {
    await this.referential.initReferential(async () => {
      let species: string | undefined =
        actor?.system?.details?.species?.value ?? undefined
      const speciesMap = ReferentialUtil.getSpeciesMap()
      let speciesKey: string = 'human'
      for (const [key, value] of Object.entries(speciesMap)) {
        if (value === species) {
          speciesKey = key
          break
        }
      }
      species = speciesMap[speciesKey]

      const model = new NpcModel()
      model.name = actor?.name ?? ''
      const careers = await this.referential.getCareerEntities(true)
      model.careers =
        actor?.itemTypes?.career
          ?.filter((c) => EntityUtil.find(c.name, careers) != null)
          ?.map((c) => EntityUtil.find(c.name, careers))
          ?.map((c) => c?.name as string) ?? []
      model.options.noRandomCaras = true
      model.options.imagePath = actor?.img ?? ''
      model.options.tokenPath = actor?.prototypeToken?.texture?.src ?? ''
      model.options.withLinkedToken = true
      model.speciesKey = speciesKey
      model.fromBuilder = new NpcModelBuilder(model)
      model.fromBuilder.skills =
        EntityUtil.duplicateWithNewIds<FoundrySkill>(
          actor?.itemTypes?.skill as FoundrySkill[],
        ) ?? []
      model.fromBuilder.talents =
        EntityUtil.duplicateWithNewIds<FoundryTalent>(
          actor?.itemTypes?.talent as FoundryTalent[],
        ) ?? []
      model.fromBuilder.traits =
        EntityUtil.duplicateWithNewIds<FoundryTrait>(
          actor?.itemTypes?.trait as FoundryTrait[],
        ) ?? []
      model.fromBuilder.psychologies =
        EntityUtil.duplicateWithNewIds<FoundryPsychology>(
          actor?.itemTypes?.psychology as FoundryPsychology[],
        ) ?? []
      model.fromBuilder.spells =
        EntityUtil.duplicateWithNewIds<FoundrySpell>(
          actor?.itemTypes?.spell as FoundrySpell[],
        ) ?? []
      model.fromBuilder.prayers =
        EntityUtil.duplicateWithNewIds<FoundryPrayer>(
          actor?.itemTypes?.prayer as FoundryPrayer[],
        ) ?? []
      model.fromBuilder.physicalMutations =
        EntityUtil.duplicateWithNewIds<FoundryMutation>(
          actor?.itemTypes?.mutation?.filter(
            (m) => m.system?.mutationType?.value === 'physical',
          ) as FoundryMutation[],
        ) ?? []
      model.fromBuilder.mentalMutations =
        EntityUtil.duplicateWithNewIds<FoundryMutation>(
          actor?.itemTypes?.mutation?.filter(
            (m) => m.system?.mutationType?.value === 'mental',
          ) as FoundryMutation[],
        ) ?? []
      model.fromBuilder.chars = {}
      for (const [charKey, charObj] of Object.entries(
        actor?.system?.characteristics ?? {},
      )) {
        model.fromBuilder.chars[charKey] = new CharModel(
          Number(charObj?.initial ?? 0),
          Number(charObj?.advances ?? 0),
        )
      }

      model.fromBuilder.move = actor?.system?.details?.move?.value ?? '0'
      model.fromBuilder.trappings =
        EntityUtil.duplicateWithNewIds<FoundryTrapping>(
          actor?.itemTypes?.trapping as FoundryTrapping[],
        ) ?? []
      model.fromBuilder.speciesValue =
        actor?.system?.details?.species?.value ?? species ?? ''
      model.fromStanding = Number(
        actor?.system?.details?.status?.standing ?? -1,
      )
      model.fromTier = actor?.system?.details?.status?.tier ?? ''
      await this.generateNpc(callback, model)
    }, true)
  }

  public static async generateNpc(
    callback?: (
      model: NpcModelBuilder,
      actorData: FoundryActor,
      actor: FoundryActor,
    ) => void,
    fromModel: NpcModel | undefined = undefined,
  ) {
    await this.referential.initReferential(async () => {
      await this.generateNpcModel(async (model) => {
        const actorData = await NpcBuilder.buildActorData(model, 'npc')
        const actor = await NpcBuilder.createActor(model, actorData)
        notifications().info(
          i18n().format('WFRP4NPCGEN.notification.actor.created', {
            name: actor.name,
          }),
        )
        await WaiterUtil.hide()
        if (callback != null) {
          callback(model, actorData, actor)
        }
      }, fromModel)
    })
  }

  public static async generateNpcModel(
    callback: (model: NpcModelBuilder) => void,
    fromModel: NpcModel | undefined = undefined,
  ) {
    const npcModel = fromModel ?? new NpcModel()
    CheckDependencies.check((canRun) => {
      if (canRun) {
        this.selectSpecies(npcModel, callback)
      }
    })
  }

  private static async selectSpecies(
    model: NpcModel,
    callback: (model: NpcModelBuilder) => void,
  ) {
    await this.speciesChooser.selectSpecies(
      model.speciesKey,
      model.subSpeciesKey,
      model.cityBorn,
      (key: string, subKey: string, cityBorn: string) => {
        if (model.speciesKey !== key || model.subSpeciesKey !== subKey) {
          model.speciesSkills = new SpeciesSkills()
          model.speciesOthers = new SpeciesOthers()
        }
        model.speciesKey = key
        model.subSpeciesKey = subKey
        model.cityBorn = cityBorn

        this.selectCareer(model, callback)
      },
    )
  }

  private static async selectCareer(
    model: NpcModel,
    callback: (model: NpcModelBuilder) => void,
  ) {
    await this.careerChooser.selectCareer(
      model.careers,
      model.speciesKey,
      model.subSpeciesKey,
      true,
      (careers: string[]) => {
        model.careers = careers

        this.selectSpeciesSkills(model, callback)
      },
      () => {
        this.selectSpecies(model, callback)
      },
    )
  }

  private static async selectSpeciesSkills(
    model: NpcModel,
    callback: (model: NpcModelBuilder) => void,
  ) {
    await this.speciesSkillsChooser.selectSpeciesSkills(
      model.speciesSkills.majors,
      model.speciesSkills.minors,
      model.speciesKey,
      model.subSpeciesKey,
      (majors: string[], minors: string[]) => {
        model.speciesSkills.majors = majors
        model.speciesSkills.minors = minors

        this.selectSpeciesOthers(model, callback)
      },
      () => {
        this.selectCareer(model, callback)
      },
    )
  }

  private static async selectSpeciesOthers(
    model: NpcModel,
    callback: (model: NpcModelBuilder) => void,
  ) {
    await this.speciesOthersChooser.selectSpeciesOthers(
      model.speciesOthers.others,
      model.speciesOthers.randomTalents,
      model.speciesOthers.originKey,
      model.speciesOthers.origin,
      model.speciesKey,
      model.subSpeciesKey,
      (
        others: string[],
        randomTalents: string[],
        originKey: string,
        origin: string[],
      ) => {
        model.speciesOthers.others = others
        model.speciesOthers.randomTalents = randomTalents
        model.speciesOthers.originKey = originKey
        model.speciesOthers.origin = origin

        this.selectName(model, callback)
      },
      () => {
        this.selectSpeciesSkills(model, callback)
      },
    )
  }

  private static async selectName(
    model: NpcModel,
    callback: (model: NpcModelBuilder) => void,
  ) {
    if (model.name == null) {
      const speciesMap = ReferentialUtil.getSpeciesMap()
      model.name = `${model.careers[model.careers.length - 1]} ${
        speciesMap[model.speciesKey]
      }`
    }
    await this.nameChooser.selectName(
      model.name,
      model.speciesKey,
      true,
      (name: string) => {
        model.name = name
        this.selectOptions(model, callback)
      },
      () => {
        this.selectSpeciesOthers(model, callback)
      },
    )
  }

  private static async selectOptions(
    model: NpcModel,
    callback: (model: NpcModelBuilder) => void,
  ) {
    await this.optionsChooser.selectOptions(
      false,
      model.options,
      model.speciesKey,
      (options: OptionsNpc) => {
        model.options = options

        NpcBuilder.buildNpc(model, callback)
      },
      () => {
        this.selectName(model, callback)
      },
    )
  }
}
